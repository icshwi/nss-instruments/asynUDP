/*  Copyright (c) 2018 - 2019    European Spallation Source ERIC
*
*  The program is free software: you can redistribute
*  it and/or modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation, either version 2 of the
*  License, or any newer version.
*
*  This program is distributed in the hope that it will be useful, 
*  but WITHOUT ANY WARRANTY; without even the implied warranty of 
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
*  See the GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License 
*  along with this program. If not, see:
*  https://www.gnu.org/licenses/gpl-2.0.txt
*
*
* AUTHOR: 
*   Tomasz Brys
*   email: tomasz.brys@esss.se
*
* DATE:    
*   Monday, January 27 2019
*   
* FILE:
*   drvasynUDP.h
*
* DESCRIPTION:
*   Driver to comunicate with FPGA
*   This version implements only memory block writing and reading
*   for a specific protocol and a specific device.
*
*/

#ifndef DRVASYNUDP_H
#define DRVASYNUDP_H

#include <iocsh.h>
#include <epicsExport.h>
#include <epicsString.h>
#include <asynPortDriver.h>
#include <asynOctetSyncIO.h>
#include <asynInt32ArraySyncIO.h>

#define NelmString	 "Nelm"         /* asynInt32      w */
#define MemoryString     "Mem"       	/* asynInt32Array rw */
#define RegisterString	 "Reg"          /* asynInt32      rw */

#define MAX_BUFF     512
static const char *driverName = "drvasynUDP";


class drvasynUDP : public asynPortDriver {
    public:
        drvasynUDP(const char *portName, const char *ip);
        
        // not sure if the destructor is necessary...
        virtual ~drvasynUDP();

        /* These are the methods that we override from asynPortDriver */
        virtual asynStatus readInt32(asynUser *pasynUser, epicsInt32 *value);
        virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
        virtual asynStatus writeInt32Array( asynUser *pasynUser, 
                epicsInt32 *value, size_t nElements);
        virtual asynStatus readInt32Array( asynUser *pasynUser, 
                epicsInt32 *value, size_t nElements, size_t *nIn);
       
        // not needed in this implementation. maybe in the future if necessary 
        //virtual void writeMemoryThread(void *);
        //virtual void readMemoryThread(void *);

	
	virtual asynStatus getAddress(asynUser *pasynUser, int *address){
	   *address = 0;
           return asynSuccess;
        }
	
    private:
        char   m_ReadBuff[MAX_BUFF];
        char   m_WriteBuff[MAX_BUFF];
        size_t m_ReadTransfer;         // number of bytes transfered
        size_t m_WriteTransfer;         // number of bytes retrived

        unsigned timeout;

        
        // library parameters
        int P_Nelm;
        int P_Mem;
        int P_Reg;

	// ???? remove ???
        char        m_IpPort[256];
        asynUser    *m_AsynUser;
	static int sequence;

        // commands for communication with the FPGA
        // initialized in the cpp file
        static unsigned char m_LoadTimeStamp[16];
        static unsigned char m_BReadInitP[16];
        static unsigned char m_BReadTermP[16];
        static unsigned char m_BWriteInitP[16];
        static unsigned char m_BWriteDataP[16];
        static unsigned char m_BWriteTermP[16];
        static unsigned char m_TimeStampRespP[16];
        static unsigned char m_CheckSumErrorP[16];
        static unsigned char m_BReadInitRespP[16];
        static unsigned char m_BReadRespP[16];
        static unsigned char m_BReadTermAckP[16];
        static unsigned char m_BWriteInitRespP[16];
        static unsigned char m_BWriteTermAckP[16];
        static unsigned char m_IndexErrorP[16];

        // support functions
        void		DEBUG_PR(char*, int pr = 0);
        int		packSumCRC8(unsigned char*, size_t len = 15);
        int 		getResponse(char*, size_t len = 16);
        void 		packD1(unsigned char*, unsigned int);
        void 		packD2(unsigned char*, unsigned int);
        void 		packIndex(unsigned char*, unsigned short);
        void 		packSequence(unsigned char*, unsigned short);
	unsigned int	unpack(char*, size_t len = 32);

	

};

#endif
