/*  Copyright (c) 2018 - 2019    European Spallation Source ERIC
*
*  The program is free software: you can redistribute
*  it and/or modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation, either version 2 of the
*  License, or any newer version.
*
*  This program is distributed in the hope that it will be useful, 
*  but WITHOUT ANY WARRANTY; without even the implied warranty of 
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
*  See the GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License 
*  along with this program. If not, see:
*  https://www.gnu.org/licenses/gpl-2.0.txt
*
*
* AUTHOR: 
*   Tomasz Brys
*   email: tomasz.brys@esss.se
*
* DATE:    
*   Monday, January 27 2019
*   
* FILE:
*   drvasynUDP.cpp
*
* DESCRIPTION:
*   Driver to comunicate with FPGA
*   This version implements only memory block writing and reading
*   for a specific protocol and a specific device.
*
*/

#include <string.h>

#include <drvasynUDP.h>
#include <unistd.h>
#include <dbAccess.h>


// Load Timestamp
unsigned char drvasynUDP::m_LoadTimeStamp[16]  = {
    0xCA, 0xFE, 0x40, 0x00, 0x0, 0x0, 0x11, 0xAA,
    0x22, 0xBB, 0x33, 0xCC, 0x0, 0x0, 0x0, 0x0 };

// Block Read Initiate Packet
unsigned char drvasynUDP::m_BReadInitP[16]     = {
    0xCA, 0xFE, 0x80, 0x00, 0x0, 0x0, 0x0, 0x0,
    0x0,  0x0,  0x0,  0x0,  0x0, 0x0, 0x0, 0x0 };
  
// Block Read Terminate Packet
unsigned char drvasynUDP::m_BReadTermP[16]     = {
    0xCA, 0xFE, 0x80, 0xC0, 0x0, 0x0, 0x11, 0xAA,
    0x0,  0x0,  0x0,  0x0,  0x0, 0x0, 0x0, 0x0 };
  
// Block Write Initiate Packet
unsigned char drvasynUDP::m_BWriteInitP[16]    = {
    0xCA, 0xFE, 0x80, 0x02, 0x0, 0x0, 0x0, 0x0,
    0x0,  0x0,  0x0,  0x0,  0x0, 0x0, 0x0, 0x0 };
  
// Block Write Data Packet
unsigned char drvasynUDP::m_BWriteDataP[16]    = {
    0xCA, 0xFE, 0x80, 0xF2, 0x0, 0x0, 0x0, 0x0,
    0x0,  0x0,  0x0,  0x0,  0x0, 0x0, 0x0, 0x0 };
  
// Block Write Terminate Packet
unsigned char drvasynUDP::m_BWriteTermP[16]    = {
    0xCA, 0xFE, 0x80, 0xC1, 0x0,  0x0,  0x11, 0xAA,
    0x0,  0x0,  0x0,  0x0,  0x0, 0x0, 0x0, 0x0 };
  
// TimeStamp Response Packet
unsigned char drvasynUDP::m_TimeStampRespP[16] = {
    0xBA, 0xBE, 0x40, 0x09, 0x0,  0x0,  0x11, 0xAA,
    0x22, 0xBB, 0x33, 0xCC, 0x44, 0xDD, 0xEE, 0x0 };
  
// Checksum Error Response Packet
unsigned char drvasynUDP::m_CheckSumErrorP[16] = {
    0xBA, 0xBE, 0xFF, 0x01, 0x0,  0x0,  0x11, 0xAA,
    0x22, 0xBB, 0x33, 0xCC, 0x44, 0xDD, 0xEE, 0x0 };
  
// Block Read Initiate Response Packet
unsigned char drvasynUDP::m_BReadInitRespP[16]  = {
    0xBA, 0xBE, 0x80, 0x80, 0x0,  0x0,  0x11, 0xAA,
    0x0,  0x0,  0x0,  0x0,  0x0, 0x0, 0x0, 0x0 };
  
// Block Read Response Packet
unsigned char drvasynUDP::m_BReadRespP[16]      = {
    0xBA, 0xBE, 0x80, 0xA0, 0x0,  0x0,  0x11, 0xAA,
    0x0,  0x0,  0x0,  0x0,  0x0, 0x0, 0x0, 0x0 };
  
// Block Read Terminate Acknowledge Packet
unsigned char drvasynUDP::m_BReadTermAckP[16]   = {
    0xBA, 0xBE, 0x80, 0xE0, 0x0,  0x0,  0x11, 0xAA,
    0x0,  0x0,  0x0,  0x0,  0x0, 0x0, 0x0, 0x0 };
  
// Block Write Initiate Response Packet
unsigned char drvasynUDP::m_BWriteInitRespP[16] = {
    0xBA, 0xBE, 0x80, 0x81, 0x0,  0x0,  0x00, 0x01,
    0x0,  0x0,  0x0,  0x0,  0x0, 0x0, 0x0, 0x0 };
  
// Block Write Terminate Acknowledge Packet
unsigned char drvasynUDP::m_BWriteTermAckP[16]  = {
    0xBA, 0xBE, 0x80, 0xD5, 0x0,  0x0,  0x11, 0xAA,
    0x0,  0x0,  0x0,  0x0,  0x0, 0x0, 0x0, 0x0 };
  
// Index Error Packet
unsigned char drvasynUDP::m_IndexErrorP[16]    = {
    0xBA, 0xBE, 0x80, 0xF1, 0x0,  0x0,  0x11, 0xAA,
    0x0,  0x0,  0x0,  0x0,  0x0, 0x0, 0x0, 0x0 };

int drvasynUDP::sequence = 0;
//-----------------------------------------------------------------------------
/** Constructor for the drvasynUDP class

*/
drvasynUDP::drvasynUDP(const char *portName, const char *ip):
  asynPortDriver(
          portName,                            // Port name 
          1,                                   // max address
          asynInt32Mask | asynInt32ArrayMask | asynDrvUserMask, 
          asynInt32Mask | asynInt32ArrayMask, 
          ASYN_MULTIDEVICE | ASYN_CANBLOCK,    // ASYN_MULTIDEVICE=1, // ASYN_CANBLOCK=1, 
          1,                                   // autoConnect=1
          0,                                   // priority 
          0),                                  // stack size
  timeout(0.5)
{
  

  asynStatus status = asynSuccess;
  const char *functionName = "drvasynUDP";

  /* Connect to desired IP port */
  //status = pasynInt32SyncIO->connect(m_IpPort, 0, &m_AsynUser, NULL);
  status = pasynOctetSyncIO->connect(ip, 0, &m_AsynUser, NULL);
  if (status) {
      printf("%s:%s: pasynOctetSyncIO->connect failure, status=%d\n", 
             driverName, functionName, status);
      return;
  }
  else{
      printf("--- %s Connection established to %s %s %i\n", 
             driverName, m_IpPort, ip, status);
  }
  
   
  createParam(NelmString,       asynParamInt32,         &P_Nelm); 
  createParam(MemoryString,     asynParamInt32Array,    &P_Mem); 
  createParam(RegisterString,   asynParamInt32,         &P_Reg); 

  //initial value for parameters
  setIntegerParam(P_Nelm, 0);                                                      
 
}

//-----------------------------------------------------------------------------
// Not sure if disconnect must be here....
drvasynUDP::~drvasynUDP(){
  pasynOctetSyncIO->disconnect(m_AsynUser);
}

//-----------------------------------------------------------------------------

asynStatus drvasynUDP::readInt32 (asynUser *pasynUser, epicsInt32 *value){

    const char*		functionName = "readInt32";
    unsigned short	index = 0;
    size_t      	nRead = 0;
    size_t      	nChars = 16;
    size_t      	nActual;
    int         	eomReason;
    int 		addr = 0;
    unsigned int tempValue = 0;
    asynStatus 		status = asynSuccess;


    int function = pasynUser->reason;
    sequence++;

    // to avoid run this function during IOC initialization
    if(!interruptAccept)
      return asynError;

    //if(function == P_Nelm){                                                               //This will never happen because there is no input record that connects to the Nelm keyword
    //   setIntegerParam(P_Nelm, *value);                                                   //This says: "Set the value of P_Nelm to the value pointed to by the pointer value". The value pointer only get's written to. 
    //}
    if (function == P_Reg) {                                                                // Means we are going to read a single register at address (&address)
       int address = 0;
       pasynManager->getAddr(pasynUser, &address);                                          //This where we read the address that we are going to read from 

       packD1(m_BReadInitP, address);
       packD2(m_BReadInitP, address);
       packIndex(m_BReadInitP, index++);
       packSequence(m_BReadInitP, sequence);
       packSumCRC8(m_BReadInitP);

       //-- send packet
       // printf("---> BlockReadInit (0x8000) address = %#x\n", address);
       // DEBUG_PR((char*)m_BReadInitP, 1);
      
       memset(m_ReadBuff, 0, sizeof(m_ReadBuff));           
       status = pasynOctetSyncIO->writeRead(m_AsynUser, 
                (char*)m_BReadInitP, nChars, 
                m_ReadBuff, sizeof(m_ReadBuff), 
                timeout, 
                &nActual, &nRead, &eomReason);

       //-- read response
       // printf("<--- BlockReadInitResponse(0x8080)\n");
       // DEBUG_PR(m_ReadBuff, 1);

       //-- response == OK
       if( !getResponse(m_ReadBuff) ){
           //-- read packet
           // printf("<--- BlockReadData (0x80A0)\n");
           
           memset(m_ReadBuff, 0, sizeof(m_ReadBuff));           
           status = pasynOctetSyncIO->read(m_AsynUser, 
                    m_ReadBuff, 32, 
                    timeout, &m_ReadTransfer, &eomReason);
           
           // DEBUG_PR(m_ReadBuff, 1);
           // if( !getResponse(mReadBuff) ){
           *value = unpack(m_ReadBuff);
           
           
           //-- prepare Block Write Terminate Packet
           // printf("---> BlockReadTerminate (0x80C0)\n");
           packIndex(m_BReadTermP, index++);
           packSequence(m_BReadTermP, sequence);
           packSumCRC8(m_BReadTermP);

           // DEBUG_PR((char*)m_BReadTermP, 1);

           memset(m_ReadBuff, 0, sizeof(m_ReadBuff));           
           status = pasynOctetSyncIO->writeRead(m_AsynUser, 
                    (char*)m_BReadTermP, nChars, 
                    m_ReadBuff, sizeof(m_ReadBuff), 
                    timeout, 
                    &nActual, &nRead, &eomReason);
           // read response
           // if( !getResponse(mReadBuff) ){ 
           //printf("<--- BlockWriteTerminateAck (0xD500)\n");
           //DEBUG_PR(m_ReadBuff, 1);
       }
    }
   
    else { // Other functions we call the base class method
       // do I need it here???? (no you don't need it)
       //status = asynPortDriver::readInt32(pasynUser, value);
    }
    
    // update callbacks so we can see changes to parameters in IOC. This might cause some problems changing all records with that parameter name, need to test
    callParamCallbacks();                                                                          

    asynPrint(pasynUser, ASYN_TRACEIO_DRIVER, 
              "%s:%s: port=%s, value=%d, addr=%d, status=%d\n",
              driverName, functionName, this->portName, *value, addr, (int)status);

    return status;                                                                                  // we assume this is for error handling
}


//-----------------------------------------------------------------------------
asynStatus drvasynUDP::writeInt32 (asynUser *pasynUser, epicsInt32 value){

    const char* functionName = "writeInt32";
    int         function     = pasynUser->reason;
    int         addr         = 0;
    size_t      nRead        = 0;
    size_t      nChars       = 16;
    size_t      nActual;
    int         eomReason;
    asynStatus	status = asynSuccess;
    int		comStatus = 0;   

    sequence++;

    if (function == P_Reg) {
       unsigned short index = 0;
       int address;

       pasynManager->getAddr(pasynUser, &address);

       packD1(m_BWriteInitP, address);
       packD2(m_BWriteInitP, address);
       packIndex(m_BWriteInitP, index++);
       packSequence(m_BWriteInitP, sequence);
       packSumCRC8(m_BWriteInitP);

       // printf("\n---> BlockWriteInit (0x8002) addr= %#x, value=%u \n", offset, value);
       // DEBUG_PR((char*)m_BWriteInitP, 1);
       
       memset(m_ReadBuff, 0, sizeof(m_ReadBuff));           
       status = pasynOctetSyncIO->writeRead(m_AsynUser, 
                (char*)m_BWriteInitP, nChars, 
                m_ReadBuff, sizeof(m_ReadBuff), 
                timeout, 
                &nActual, &nRead, &eomReason);

       //-- read response
       // printf("<--- BlockWriteInitResponse(8081)\n");
       // DEBUG_PR(m_ReadBuff, 1);
       comStatus = getResponse(m_ReadBuff);
       if( comStatus == 0 ){
           //-- prepare Block Write Data (one register)
           // printf("---> BlockWriteData (0x80F2), data = %u\n", value);
           packD2(m_BWriteDataP, value);
           packIndex(m_BWriteDataP, index++);
           packSequence(m_BWriteDataP, sequence);
           packSumCRC8(m_BWriteDataP);

           // DEBUG_PR((char*)m_BWriteDataP, 1);
           
           //-- send packet
           status = pasynOctetSyncIO->write(m_AsynUser, 
                    (char*)m_BWriteDataP, 32, 
                    timeout, &m_WriteTransfer);
           
           //-- prepare Block Write Terminate Packet
           // printf("---> BlockWriteTerminate (0x80C1)\n");
           packIndex(m_BWriteTermP, index++);
           packSequence(m_BWriteTermP, sequence);
           packSumCRC8(m_BWriteTermP);

           // DEBUG_PR((char*)m_BWriteTermP, 1);
           
           memset(m_ReadBuff, 0, sizeof(m_ReadBuff));           
           status = pasynOctetSyncIO->writeRead(m_AsynUser, 
                    (char*)m_BWriteTermP, nChars, 
                    m_ReadBuff, sizeof(m_ReadBuff), 
                    timeout, 
                    &nActual, &nRead, &eomReason);
           comStatus = getResponse(m_ReadBuff);
           if( comStatus > 0 ){
           }
           //-- read response
           // printf("<--- BlockWriteTerminateAck (0xD500)\n");
           // DEBUG_PR(m_ReadBuff, 1);
       }
    }
    else { // Other functions we call the base class method
        //status = asynPortDriver::writeInt32(pasynUser, value);
    }

    /* Do callbacks so higher layers see any changes */
    status = (asynStatus) callParamCallbacks();

    if(status){
    epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize,
                        "%s:%s: status=%d, function=%d, value=%d",
                        driverName, functionName, status, function, value);
    }
    else{
    asynPrint(pasynUser, ASYN_TRACEIO_DRIVER, "%s:%s: port=%s, value=%d, addr=%d, status=%d\n",
            driverName, functionName, this->portName, value, addr, (int)status);
    }
    return status;
}

//-----------------------------------------------------------------------------
asynStatus drvasynUDP::readInt32Array(asynUser *pasynUser, 
           epicsInt32 *value, size_t nElements, size_t *nIn) {

    const char *functionName = "readInt32Array";
    int		index = 0;
    int		addr = 0;
    size_t	nActual = 0;
    size_t      nRead = 0;
    size_t      nChars = 16;
    int         eomReason;
    unsigned int readValue;
    asynStatus status = asynSuccess;
    int		comStatus = 0;   

    int address1;
    int address2;
    
    int function = pasynUser->reason;
    sequence++;
    pasynManager->getAddr(pasynUser, &address1);
    int nelm; 
    
    getIntegerParam(P_Nelm, &nelm);

    if((size_t)nelm > nElements){
      printf("--- WARNING --- nelm > nElements\n");
      printf("--- WARNING --- set nelm == nElements\n");
      nelm = nElements;
    }

    address2 = address1 + 4*nelm - 4;
    
    if (function == P_Mem) {
       packD1(m_BReadInitP, address1);
       packD2(m_BReadInitP, address2);
       packIndex(m_BReadInitP, index++);
       packSequence(m_BReadInitP, sequence);
       packSumCRC8(m_BReadInitP);
       // printf("---> BlockReadInit (0x8000) address1= %#x, address2=%#x \n", address1, address2);
       // DEBUG_PR((char*)m_BReadInitP, 1);
      
       memset(m_ReadBuff, 0, sizeof(m_ReadBuff));           
       status = pasynOctetSyncIO->writeRead(m_AsynUser, 
                (char*)m_BReadInitP, nChars, 
                m_ReadBuff, sizeof(m_ReadBuff), 
                timeout, 
                &nActual, &nRead, &eomReason);

       comStatus = getResponse(m_ReadBuff);
       if( comStatus == 0 ){
          // printf("<--- BlockReadInitResponse(0x8080)\n");
          // DEBUG_PR(m_ReadBuff, 1);
          // printf("<--- BlockReadData (0x80A0)\n");

          size_t i = 0;    
          for(i = 0; i < (size_t)nelm; i++){
              memset(m_ReadBuff, 0, sizeof(m_ReadBuff));           
              status = pasynOctetSyncIO->read(m_AsynUser, 
                       m_ReadBuff, 32, 
                       timeout, &m_ReadTransfer, &eomReason);
              if(status){
                 break;
                 DEBUG_PR(m_ReadBuff, 1);
              }
              else{
                  comStatus = getResponse(m_ReadBuff);
                  readValue = unpack(m_ReadBuff, 32);
                  // DEBUG_PR(m_ReadBuff, 1);
                  // printf("--- [DEBUG] unpack m_ReadBuff = %u\n", unpack(m_ReadBuff, m_ReadTransfer));
                   value[i] = readValue;
              }
          }
        
          *nIn = i;
     
          // prepare Block Write Terminate Packet
          // printf("---> BlockReadTerminate (0x80C0)\n");
          packIndex(m_BReadTermP, index++);
          packSequence(m_BReadTermP, sequence);
          packSumCRC8(m_BReadTermP);

          // DEBUG_PR((char*)m_BReadTermP, 1);
          memset(m_ReadBuff, 0, sizeof(m_ReadBuff));           
          status = pasynOctetSyncIO->writeRead(m_AsynUser, 
                   (char*)m_BReadTermP, nChars, 
                   m_ReadBuff, sizeof(m_ReadBuff), 
                   timeout, 
                   &nActual, &nRead, &eomReason);
          // read response
          // printf("<--- BlockWriteTerminateAck (0xD500)\n");
          // DEBUG_PR(m_ReadBuff, 1);
          comStatus = getResponse(m_ReadBuff);
          }
       }
       else { // Other functions we call the base class method
          //  status = asynPortDriver::readInt32Array(pasynUser,
          //     epicsInt32 *value, size_t nElements, size_t *nIn);
       }

    /* Do callbacks so higher layers see any changes */
    status = (asynStatus) callParamCallbacks();
    if(status){
    epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize,
                        "%s:%s: status=%d, function=%d, value=---",
                        driverName, functionName, status, function);
    }
    else{
    asynPrint(pasynUser, ASYN_TRACEIO_DRIVER, "%s:%s: port=%s, value=---, addr=%d, status=%d\n",
            driverName, functionName, this->portName, addr, (int)status);
    }
    return status;

}
//-----------------------------------------------------------------------------
asynStatus drvasynUDP::writeInt32Array(asynUser *pasynUser, 
           epicsInt32 *value, size_t nElements){

    const char*	functionName    = "writeInt32Array";
    asynStatus	status          = asynSuccess;
    int		function        = pasynUser->reason;
    int		addr 		= 0;
    size_t	nActual 	= 0;
    size_t	nChars 		= 16;
    size_t	nRead		= 0;
    int		eomReason;
    
    int address1;
    int address2;

    sequence++;
    pasynManager->getAddr(pasynUser, &address1);
    address2 = address1 + 4*nElements - 4;
    // printf("--- [DEBUG] writeInt32, offset = %#x, offset2 = %#x\n", offset, offset2);
    // printf("--- [DEBUG] %s:%s \n", driverName, functionName);
    // printf("--- nElements=%zu\n", nElements);

    if (function == P_Mem) {
       unsigned short index = 0;
        
       packD1(m_BWriteInitP, address1);
       packD2(m_BWriteInitP, address2);
       packIndex(m_BWriteInitP, index++);
       packSequence(m_BWriteInitP, sequence);
       packSumCRC8(m_BWriteInitP);
       // printf("\n---> BlockWriteInit (0x8002) addr= %#x, addr2=%#x \n", offset, offset2);
       // DEBUG_PR((char*)m_BWriteInitP, 1);

       memset(m_ReadBuff, 0, sizeof(m_ReadBuff)); 
       status = pasynOctetSyncIO->writeRead(m_AsynUser, 
                (char*)m_BWriteInitP, nChars, 
                m_ReadBuff, sizeof(m_ReadBuff), 
                timeout, 
                &nActual, &nRead, &eomReason);

       //-- read response
       // printf("<--- BlockWriteInitResponse(8081)\n");
       // DEBUG_PR(m_ReadBuff, 1);

       if( !getResponse(m_ReadBuff) ){
           // prepare Block Write Data (one register)
           for(size_t i = 0; i < nElements; i++){
              //printf("---> BlockWriteData (0x80F2), data[%lu] = %u\n", i, value[i]);
              packD2(m_BWriteDataP, value[i]);
              packIndex(m_BWriteDataP, index++);
              packSequence(m_BWriteDataP, sequence);
              packSumCRC8(m_BWriteDataP);

              // DEBUG_PR((char*)m_BWriteDataP, 1);
           
              memset(m_ReadBuff, 0, sizeof(m_ReadBuff)); 
              status = pasynOctetSyncIO->write(m_AsynUser, 
                        (char*)m_BWriteDataP, 32, 
                        timeout, &m_WriteTransfer);

              if(status) 
                  break;
              //if( !getResponse(m_ReadBuff) )
              //    break;
           }
           //-- prepare Block Write Terminate Packet
           // printf("---> BlockWriteTerminate (0x80C1)\n");
           memset(m_ReadBuff, 0, sizeof(m_ReadBuff)); 
           packIndex(m_BWriteTermP, index++);
           packSequence(m_BWriteTermP, sequence);
           packSumCRC8(m_BWriteTermP);

           //DEBUG_PR((char*)m_BWriteTermP, 1);
           
           memset(m_ReadBuff, 0, sizeof(m_ReadBuff)); 
           status = pasynOctetSyncIO->writeRead(m_AsynUser, 
                    (char*)m_BWriteTermP, nChars, 
                    m_ReadBuff, sizeof(m_ReadBuff), 
                    timeout, 
                    &nActual, &nRead, &eomReason);

           //if( !getResponse(m_ReadBuff) )
           //    break;
           // printf("<--- BlockWriteTerminateAck (0xD500)\n");
           // DEBUG_PR(m_ReadBuff, 1);
       }

    }
    else { // Other functions we call the base class method
       status = asynPortDriver::writeInt32Array(pasynUser, value, nElements);
    }

    /* Do callbacks so higher layers see any changes */
    status = (asynStatus) callParamCallbacks();

    if(status){
    epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize,
                        "%s:%s: status=%d, function=%d, value=---",
                        driverName, functionName, status, function);
    }
    else{
    asynPrint(pasynUser, ASYN_TRACEIO_DRIVER, "%s:%s: port=%s, value=---, addr=%d, status=%d\n",
            driverName, functionName, this->portName, addr, (int)status);
    }
    return status;
}

//-----------------------------------------------------------------------------
int drvasynUDP::getResponse(char* tab, size_t len){
   return 0;
   // I have to define those variables 
   // otherwise missmach between signed and unsigned
   // I have FFFFFF before the real value e.g. tab[0] = 0xFFFFFFBA
   //
   unsigned char tab0 = tab[0] & 0xFF;
   unsigned char tab1 = tab[1] & 0xFF;
   unsigned char tab2 = tab[2] & 0xFF;
   unsigned char tab3 = tab[3] & 0xFF;
   unsigned char tab15 = tab[15] & 0xFF;

   // check header
   if(tab0 != 0xBA && tab1 != 0xBE){
     printf("--- ERROR: header, expected 0xBABE, got 0x%02x%02x\n",tab0, tab1);
     //return 1;
   }
   // check command
   if(tab2 != 0x40 && tab3 != 0x09){
     printf("--- ERROR: Time stamp response packet, wrong command\n");
     printf("--- expected 0x4009, got 0x%x%x\n", tab2, tab3);
     //return 2;
   }
   else if(tab2 != 0x80 && tab3 != 0x80){
     printf("--- ERROR: Block Read Init Response Packet, wrong command\n");
     printf("--- expected 0x8080, got 0x%x%x\n", tab2, tab3);
     //return 3;
   }
   else if(tab2 != 0x80 && tab3 != 0xA0){
     printf("--- ERROR: Block Read Response Packet, wrong command\n");
     printf("--- expected 0x80A0, got 0x%x%x\n", tab2, tab3);
     //return 4;
   }
   else if(tab2 != 0x80 && tab3 != 0xE0){
     printf("--- ERROR: Block Read Terminate Acknowledge Packet, wrong command\n");
     printf("--- expected 0x80E0, got 0x%x%x\n", tab2, tab3);
     //return 5;
   }
   else if(tab2 != 0x80 && tab3 != 0x81){
     printf("--- ERROR: Block Write Init Response Packet, wrong command\n");
     printf("--- expected 0x8081, got 0x%x%x\n", tab2, tab3);
     //return 6;
   }
   else if(tab2 != 0x80 && tab3 != 0xD5){
     printf("--- ERROR: Block Write Terminate Acknowledge Packet, wrong command\n");
     printf("--- expected 0x80D5, got 0x%x%x\n", tab2, tab3);
     //return 7;
   }
   else if(tab2 == 0x80 && tab3 == 0xF1){
     printf("--- ERROR: Index Packet\n");
     printf("--- expected 0x4009, got 0x%x%x\n", tab2, tab3);
     //return 8;
   }
   else if(tab2 == 0xFF && tab3 == 0x01){
     printf("--- ERROR: Time stamp response packet\n");
     printf("--- expected 0x4009, got 0x%x%x\n", tab2, tab3);
     //return 9;
   }
   // check sum8
   int  sum8 = packSumCRC8((unsigned char*)tab, 15);
   if(tab15 != sum8){
     printf("--- ERROR: crc sum, expected 0x%02x, got 0x%02x\n",tab15, sum8);
     //return 10;
   }
   
return 0;
}
//-----------------------------------------------------------------------------
int drvasynUDP::packSumCRC8(unsigned char* tab, size_t len){

    unsigned int sum8 = 0;

    //tab[15] = 0x0;
    while (len--) {
        sum8 += *tab++;
    }

    *tab = sum8;
    // printf("tab = %#02x\n",*tab);
    // printf(" [DEBUG] drvasynUDP::packSumCRC8 crc = %#x\n", sum8 );
return sum8 & 0xFF;    
}

//-----------------------------------------------------------------------------
unsigned int drvasynUDP::unpack(char* d1, size_t len){
   unsigned int d;
   

   
   d  = (unsigned char)d1[4] << 24;
   d += (unsigned char)d1[5] << 16;
   d += (unsigned char)d1[6] << 8;
   d += (unsigned char)d1[7] ;
   
   
return d;
}
//-----------------------------------------------------------------------------
void drvasynUDP::packD1(unsigned char* d1, unsigned int addr){
   
  d1[4] = (addr & 0xFF000000) >> 24; 
  d1[5] = (addr & 0xFF0000  ) >> 16;
  d1[6] = (addr & 0xFF00    ) >> 8; 
  d1[7] = (addr & 0xFF      );
  //unsigned int* ptr = (unsigned int*)d1;
  //*(ptr + 1) = addr;
   
}
//-----------------------------------------------------------------------------
void drvasynUDP::packD2(unsigned char* d2, unsigned int addr){

  d2[8]  = (addr & 0xFF000000) >> 24; 
  d2[9]  = (addr & 0xFF0000  ) >> 16;
  d2[10] = (addr & 0xFF00    ) >> 8; 
  d2[11] = (addr & 0xFF      );
}
//-----------------------------------------------------------------------------
void drvasynUDP::packIndex(unsigned char* buff, unsigned short index){

   buff[12] = (index & 0xFF00) >> 8;
   buff[13] = (index & 0xFF);
}
//-----------------------------------------------------------------------------
void drvasynUDP::packSequence(unsigned char* buff, unsigned short index){

   buff[14] = (index & 0xFF);
}
//-----------------------------------------------------------------------------
void drvasynUDP::DEBUG_PR(char *buff, int pr){

unsigned char* ptr = (unsigned char*)buff;

   if(pr == 0){
      printf("---    data:\t");
      for(size_t i = 0; i < 4; i++){
          printf("%#02x, ", *(ptr + i));
      }
      printf(" crc= %#02x\n", *(ptr+15));
   }
   else{
      printf("     DEBUG: ");
      for(size_t i = 0; i < 16; i++){
          if(i < 15)
            printf("%02x:", *(ptr + i));
          else
            printf("%02x", *(ptr + i));
      }
      printf("\n");
   }
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EPICS configuration part
//-----------------------------------------------------------------------------

/** Configuration command, called directly or from iocsh */
extern "C"{ 
   static void drvasynUDPConfigure(const char *portName, const char *ip) {
      new drvasynUDP(portName, ip);
   }

   static const iocshArg configArg0 = { "portName",   iocshArgString};
   static const iocshArg configArg1 = { "ip",         iocshArgString};
   static const iocshArg * const configArgs[] = {
       &configArg0,
       &configArg1,
   };

   static const iocshFuncDef drvasynUDPconfigFuncDef = {
       "drvasynUDPConfig", 
       2, 
       configArgs
   };

   static void drvasynUDPconfigFunc(const iocshArgBuf *args) {
     drvasynUDPConfigure(args[0].sval, args[1].sval);
   }

   void drvasynUDPRegister(void) {
     iocshRegister(&drvasynUDPconfigFuncDef, drvasynUDPconfigFunc);
   }

   epicsExportRegistrar(drvasynUDPRegister);
} // end of extern "C"

